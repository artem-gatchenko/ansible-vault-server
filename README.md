# Ansible - Vault server

## Installation
### Requrements
ansible >= 2.7
allowed port - 8200

### Pre-install
Copy `hosts.sample` file with name `hosts`
Set the IP address into `hosts` file and Ansible user

### Install
```bash
ansible-playbook vault.yaml
```

And follow instruction from output.

## Connect with Terraform
### Enable AppRole auth method
You can enable approle auth via Web UI, just follow -> *Access* -> *Auth Methods* -> *Enable new method* and select *AppRole*

Also you can enable it via console:
```bash
vault auth enable approle
```

### Policy
Add this rule to `default` policy.
```hcl
# Allow AppRole auth
path "auth/token/create" {
    capabilities=["update"]
}
```

### Creating KV
You can create KV via Web UI or via console:

```bash
vault secrets enable -path=my_secret kv-v1
```

Where *kv-v1* this version of KV. (kv-v1|kv-v2)

### ReadOnly policy
Path depends of KV version. For v2 you must set "data" in your path.
```hcl
# KV v1
path "my_secret/*" {
  capabilities = [ "read", "list" ]
}

# KV v2
path "my_secret/data/*" {
  capabilities = [ "read", "list" ]
}
```

### Create AppRole
AppRole for terraform with name `terraform` with attached `ReadOnly` policy
```bash
vault write auth/approle/role/terraform \
		token_policies="my_secret-readonly" \
        token_ttl=1h \
        token_max_ttl=4h
```

Look AppRole `terraform` ID:
```bash
vault read auth/approle/role/terraform/role-id
```

And Secret ID:
```bash
vault write -f auth/approle/role/artem/secret-id
```

### Create secret
For example we are create secret with name `my_credentials` and put on it 2 keys:
```bash
vault kv put my_secret/my_credentials login=foo
vault kv put my_secret/my_credentials password=bar
```


### Terraform - Example
*main.tf*
```tf
provider "vault" {
  address = var.vault_address

  auth_login {
    path = "auth/approle/login"

    parameters = {
      role_id   = var.login_approle_role_id
      secret_id = var.login_approle_secret_id
    }
  }
}

# Mount secret from vault
resource "vault_generic_secret" "vault" {
  path = "my_secret/my_credentials"

}


output "login" { value = vault_generic_secret.vault.data["login"] }
output "password" { value = vault_generic_secret.vault.data["password"] }
```

*variables.tf*
```tf
variable "login_approle_role_id" { default = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" }
variable "login_approle_secret_id" { default = "yyyyyyyy-yyyy-yyyy-yyyy-yyyyyyyyyyyy" }
variable "vault_address" { default = "http://vault.example.com:8200" }
```